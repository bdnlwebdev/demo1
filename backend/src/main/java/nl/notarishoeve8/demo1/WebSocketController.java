package nl.notarishoeve8.demo1;

import lombok.extern.log4j.Log4j2;
import nl.notarishoeve8.demo1.rss.Feeddata;
import nl.notarishoeve8.demo1.rss.RssFeedProcessingException;
import nl.notarishoeve8.demo1.rss.RssService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.handler.annotation.MessageExceptionHandler;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.socket.messaging.SessionSubscribeEvent;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Controller
@Log4j2
public class  WebSocketController {

    private ColumnData[][] columnData = new ColumnData[][] {
            new ColumnData[] { new ColumnData("Ja", 0), new ColumnData("Beetje", 0), new ColumnData("Nee", 0) },
            new ColumnData[] { new ColumnData("1", 0), new ColumnData("2", 0), new ColumnData("3", 0), new ColumnData("4", 0), new ColumnData("5", 0) },
            new ColumnData[] { new ColumnData("1", 0), new ColumnData("2", 0), new ColumnData("3", 0), new ColumnData("4", 0), new ColumnData("5", 0),
                    new ColumnData("6", 0), new ColumnData("7", 0), new ColumnData("8", 0), new ColumnData("9", 0), new ColumnData("10", 0)},
            new ColumnData[] { new ColumnData("Ja", 0), new ColumnData("Nee", 0) }
    };

    private ChartData[] chartData = new ChartData[] {
            new ChartData("Ben je bekend met WebSockets?","Enquete","Ja, Nee, Beetje", columnData[0]),
            new ChartData("Hoe is je kennis mbv Java?","Enquete","1 ... 10", columnData[2]),
            new ChartData("Hoe is je kennis mbv Angular?","Enquete","1 ... 10", columnData[2]),
            new ChartData("Hoe is je kennis mbv Sprintboot?","Enquete","1 ... 5", columnData[1]),
            new ChartData("Heb je ervaring met OpenShift?","Enquete","Ja, Nee, Beetje", columnData[0]),
            new ChartData("Heb je ervaring met/kennis van Firebase?","Enquete","Ja, Nee", columnData[3])
    };

    private List<String> users = new ArrayList<>();
    private ColumnData[] currentColumnData;
    private ChartData currentChartData;

    private Tabs tabs;

    public WebSocketController() {

    }

    @Autowired
    private SimpMessageSendingOperations messagingTemplate;

    @Autowired
    private Counter counter;

    @Autowired
    private RssService rssService;

    @MessageMapping("/message")
    @SendTo("/topic/message")
    public String processMessageFromClient(@Payload String message) throws Exception {
        return message;
    }

    @MessageMapping("/counter")
    @SendTo("/topic/counter")
    public Integer processCounterFromClient(@Payload String direction) throws Exception {
        return direction.equalsIgnoreCase("up") ? counter.up() : counter.down();
    }

    @MessageMapping("/stemmen")
    @SendTo("/topic/stemmen")
    public ColumnData[] processStemmen(@Payload Integer column) throws Exception {
        currentColumnData[column].value++;
        return currentColumnData;
    }

    @MessageMapping("/chartConfig")
    @SendTo("/topic/chartConfig")
    public ChartData selectChart(@Payload Integer type) throws Exception {
        currentColumnData = cloneColumnData(chartData[type].data);
        currentChartData = chartData[type];
        return chartData[type];
    }

    @MessageMapping("/user")
    @SendTo("/topic/users")
    public List<String> userLoggedOn(@Payload String user) throws Exception {
        users.add(user);
        Collections.sort(users);
        if (tabs != null) {
            messagingTemplate.convertAndSend("/topic/tabs", tabs);
        }
        return users;
    }

    @MessageMapping("/tabs")
    @SendTo("/topic/tabs")
    public Tabs echoTabs(@Payload Tabs tabs) throws Exception {
        this.tabs = tabs;
        return tabs;
    }

    @MessageMapping("/userLoggedOff")
    @SendTo("/topic/users")
    public List<String> userLoggedOff(@Payload String user) throws Exception {
        users.remove(user);
        Collections.sort(users);
        return users;
    }

    private ColumnData[] cloneColumnData(ColumnData[] columnDatum) {
        ColumnData[] result = new ColumnData[columnDatum.length];
        for(int x = 0; x < columnDatum.length; x++) {
            result[x] = new ColumnData(columnDatum[x].label, 0);
        }
        return result;
    }

    @MessageExceptionHandler
    @SendTo("/topic/errors")
    public String handleException(Throwable exception) {
        return exception.getMessage();
    }

    private int previousCounterValue = -1;
    @Scheduled(initialDelay = 5000, fixedRate = 5000)
    private void DecreaseTimer() {
        int newValue = counter.decay();
        if (previousCounterValue != 0 || newValue != 0) {
            previousCounterValue = newValue;
            messagingTemplate.convertAndSend("/topic/counter", newValue);
        }
    }

    @Scheduled(initialDelay = 10000, fixedRate = 60000)
    private void readRssFeed() {
        Feeddata feedData = null;
        try {
            feedData = rssService.getFeeddata();
            if (feedData != null) {
                messagingTemplate.convertAndSend("/topic/news", feedData);
            }
        } catch (RssFeedProcessingException e) {
            log.error("Problem processing rss feed.", e);
            handleException(e);
        }
    }

    @EventListener
    public void handleSessionSubscribeEvent(SessionSubscribeEvent event) {
        GenericMessage message = (GenericMessage) event.getMessage();
        String simpDestination = (String) message.getHeaders().get("simpDestination");
        if (simpDestination.startsWith("/topic/news")) {
            readRssFeed();
        }
        else if (simpDestination.startsWith("/topic/chartConfig")) {
            if (currentChartData != null) {
                ChartData data = new ChartData(currentChartData.question, currentChartData.chart.caption, currentChartData.chart.subCaption, currentColumnData);
                messagingTemplate.convertAndSend("/topic/chartConfig", data);
            }
        }
    }
}

class ChartData {
    public ChartCaption chart = new ChartCaption();
    public ColumnData[] data =new ColumnData[] {new ColumnData("Ja", 18), new ColumnData("Beetje", 7), new ColumnData("Nee", 5), };
    public String question;

    public ChartData(String question, String title, String subTitle, ColumnData[] columnDatum) {
        this.question = question;
        this.chart.caption = title;
        this.chart.subCaption = subTitle;
        this.data = columnDatum;
    }
}
class ChartCaption {
    public String caption = "Enquete";
    public String subCaption = "bla die bla";
    public String xAxisName = "Keuzes";
    public String yAxisName = "aantal";
    public String numberSuffix = "";
    public String theme = "fusion";
}
class ColumnData {
    public String label;
    public int value = 0;

    public ColumnData() {
    }

    public ColumnData(String label, int v) {
        this.label = label;
        this.value = v;
    }
}

class Tabs {
    public boolean user;
    public boolean message; 
    public boolean stemmen; 
    public boolean nieuws; 
    public boolean snelheid;
}
