import { Component, OnInit } from '@angular/core';
import { RxStompService } from '@stomp/ng2-stompjs';
import { Message } from '@stomp/stompjs';
import { Subscription, Observable } from 'rxjs';
import { UserService } from '../service/user.service';

export interface ChartType {
  index: number;
  name: string;
}

@Component({
  selector: 'app-stemmen',
  templateUrl: './stemmen.component.html',
  styles: [
    '.stemmenVraag: { font-size: 18px; }' 
  ]
})

export class StemmenComponent implements OnInit {

  private stemmenSubscription: Subscription;
  private stemmenConfigSubscription: Subscription;
  public dataSource: any;
  public chartConfig: any;
  private chartData: any;
  public question = "--"

  // chartTypes: ChartType[] = [
  //   {name: 'Ja Nee Beetje', index: 0},
  //   {name: '1 ... 5', index: 1},
  //   {name: '1 ... 10', index: 2},
  //   {name: 'Ja Nee', index: 3},
  // ];
  chartTypes: ChartType[] = [
    {name: 'Websockets', index: 0},
    {name: 'Java', index: 1},
    {name: 'Angular', index: 2},
    {name: 'Springboot', index: 3},
    {name: 'OpenShift', index: 4},
    {name: 'Firebase', index: 5},
  ];


  constructor(public rxStompService: RxStompService, public userService: UserService) {
    this.chartConfig = {
      width: '90%',
      height: '100%',
      type: 'column2d',
      dataFormat: 'json',
    };

    this.chartData = [{
        "label": "Ja",
        "value": "18"
      }, {
        "label": "Beetje",
        "value": "7"
      }, {
        "label": "Nee",
        "value": "5"
    }];
    this.dataSource = this.buildDataSource(this.chartData);

  }

  ngOnInit() {
    this.stemmenSubscription = this.rxStompService.watch('/topic/stemmen').subscribe((message: Message) => {
      console.log("stemmen: " + message.body);
      let data = JSON.parse(message.body);
      this.dataSource = this.buildDataSource(data);
    });
    this.stemmenConfigSubscription = this.rxStompService.watch('/topic/chartConfig').subscribe((message: Message) => {
      console.log("chartConfig: " + message.body);
      let config = JSON.parse(message.body);
      this.dataSource = config;
      this.question = this.dataSource.question;
    });
  }
  ngOnDestroy() {
    this.stemmenConfigSubscription.unsubscribe();
  }

  onStemmen(col: number) {
    console.log("onStemmen " + col);
    this.rxStompService.publish({destination: '/app/stemmen', body: col.toLocaleString()});
  }
  onStemmenConfig(type: number) {
    console.log("onStemmenConfig " + type);
    this.rxStompService.publish({destination: '/app/chartConfig', body: type.toLocaleString()});
  }
  buildDataSource(data: []) {
    return {
      "chart": {
        "caption": "Enquete",
        "subCaption": "bla die bla",
        "xAxisName": "Keuzes",
        "yAxisName": "aantal",
        "numberSuffix": "",
        "theme": "fusion",
      },
      "data": data 
    };
  }

}
