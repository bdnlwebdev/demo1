Pas de pagina aan zodat je:

1) de laatste 15 documenten uit de "nieuws" collectie op het scherm toont
2) Wanneer er nieuws bij komt, dit zichtbaar wordt
3) Laat zien wanneer er voor het laatst een wijziging in de collectie geweest is

Gebruik hiervoor de functies die al in de pagina aanwezig zijn.

Info is te vinden op https://firebase.google.com/docs/firestore