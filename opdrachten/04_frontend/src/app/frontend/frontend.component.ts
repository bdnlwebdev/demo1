import { Component, OnInit, OnDestroy } from '@angular/core';
import { RxStompService } from '@stomp/ng2-stompjs';
import { Message } from '@stomp/stompjs';
import { Subscription, Observable } from 'rxjs';
import {RxStompState} from '@stomp/rx-stomp';
import {map} from 'rxjs/operators';
import {FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-frontend',
  templateUrl: './frontend.component.html',
  styles: [
       '#indicator { display: inline-block; }'
    ]
})
export class FrontendComponent implements OnInit, OnDestroy {


  public message2Send : string;
  public connectionStatus$: Observable<string>;
  public receivedMessages: string[] = [];
  public timer: string;
  private timerSubscription: Subscription;
  private messagesSubscription: Subscription;

  constructor(public rxStompService: RxStompService) { 
    this.connectionStatus$ = rxStompService.connectionState$.pipe(map((state) => {
      // convert numeric RxStompState to string
      return RxStompState[state];
    }));
  }

  ngOnInit() {
  	<!-- Subscribe to timer topic and shpw the time on the page -->
    this.timerSubscription = 
    ........
    ;
    <!-- Subscribe to messages topic and display the messages on the page -->
    this.messagesSubscription = 
    ........
    ;
  }

  ngOnDestroy() {
	<!-- Unsubscribe from both topics -->
	........
  }
  
  sendMessage() {
  	<!-- Send the message to the server
    this.rxStompService.publish
    .....................................
    ;
  }

}
